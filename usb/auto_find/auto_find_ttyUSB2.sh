#!/bin/bash

# Traversing /sys/bus/usb/devices
for usb_device in /sys/bus/usb/devices/* ; do
    if [ -e "$usb_device/idVendor" ] && [ -e "$usb_device/idProduct" ] ; then
        vid=$(cat "$usb_device/idVendor")
        pid=$(cat "$usb_device/idProduct")
	if [ "$vid" = "2c7c" ]; then
		for usb_if in $usb_device/*; do
			if [ -d $usb_if ] && [ -e "$usb_if/bInterfaceNumber" ] ; then
		        	intf=$(cat "$usb_if/bInterfaceNumber")
				if [  "$intf" = "02" ]; then
			        #echo "USB Device found:"
			        #echo "  Interface Number: $intf"
					ttyfile=$(find  "$usb_if"  -maxdepth 1 -type d  -name 'ttyUSB*' -print -quit)
					#echo "$ttyfile"
				fi
			fi
		done
	fi
    fi
done

if [ -n "$ttyfile" ]; then
	echo The target is $(basename "$ttyfile")
fi
	
