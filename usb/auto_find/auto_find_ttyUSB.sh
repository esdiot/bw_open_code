#!/bin/bash

# Vendor ID and Product ID for the USB device
VENDOR_ID="2c7c"
PRODUCT_ID="0901"

# Iterate through each /dev/ttyUSB* device
for SERIAL_PORT in /dev/ttyUSB*; do
    # Use udevadm to get attributes for the device
    USB_INFO1=$(udevadm info -a -n $SERIAL_PORT | grep "$VENDOR_ID")
    USB_INFO2=$(udevadm info -a -n $SERIAL_PORT | grep "$PRODUCT_ID")
    intf=$(  udevadm info -a -n $SERIAL_PORT | grep -Po 'ATTRS{bInterfaceNumber}=="\K[^"]+' )
   # USB_INFO=$(udevadm info -a -n $SERIAL_PORT | grep -A 2 "$VENDOR_ID:$PRODUCT_ID")

    # Check if the USB device is connected
    if [ -n "$USB_INFO1" ] && [ -n "$USB_INFO2" ]; then
	#echo $intf
	if  [ "$intf" = "03" ]; then
        echo "USB Serial Port for $VENDOR_ID:$PRODUCT_ID: $SERIAL_PORT"
        exit 0  # Exit after finding the first matching device
	fi
    fi
done

# If no matching device is found
echo "No /dev/ttyUSB* node found for $VENDOR_ID:$PRODUCT_ID."
exit 1

