#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <strings.h>
#include <stdlib.h>
#include <limits.h>
#include <linux/usbdevice_fs.h>
#include <linux/types.h>
#include <net/if.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
typedef int   BOOL;

#define dbg_time(fmt, args...) do { \
    fprintf(stdout,  fmt "\n", ##args); \
        fflush(stdout); \
} while(0)

#define TRUE (1 == 1)
#define FALSE (1 != 1)

enum
{
        DRV_INVALID,
        SOFTWARE_ECM_RNDIS_NCM,
        HARDWARE_USB,
};

#define USB_CLASS_VENDOR_SPEC		0xff
#define USB_CLASS_COMM			2
#define USB_CDC_SUBCLASS_ACM			0x02
#define USB_CDC_SUBCLASS_ETHERNET       0x06
#define USB_CDC_SUBCLASS_NCM			0x0d
#define USB_CLASS_WIRELESS_CONTROLLER	0xe0

#define CM_MAX_PATHLEN 256

#define CM_INVALID_VAL (~((int)0))

struct usb_device_info {
    int idVendor;
    int idProduct;
    int  busnum;
    int devnum;
    int bNumInterfaces;
};

struct usb_interface_info {
    int bNumEndpoints;
    int bInterfaceClass;
    int bInterfaceSubClass;
    int bInterfaceProtocol;
    char driver[32];
};

typedef struct __PROFILE {

    char qmichannel[32];
    char usbnet_adapter[32];
    char qmapnet_adapter[32];
    char driver_name[32];

//    bool reattach_flag;
    int hardware_interface;
    int software_interface;

    struct usb_device_info usb_dev;
    struct usb_interface_info usb_intf;

} PROFILE_T;

int get_driver_type(PROFILE_T *profile);
static PROFILE_T s_profile;

/* get first line from file 'fname'
 * And convert the content into a hex number, then return this number */
static int file_get_value(const char *fname, int base)
{
    FILE *fp = NULL;
    long num;
    char buff[32 + 1] = {'\0'};
    char *endptr = NULL;

    fp = fopen(fname, "r");
    if (!fp) goto error;
    if (fgets(buff, sizeof(buff), fp) == NULL)
        goto error;
    fclose(fp);

    num = (int)strtol(buff, &endptr, base);
    if (errno == ERANGE && (num == LONG_MAX || num == LONG_MIN))
        goto error;
    /* if there is no digit in buff */
    if (endptr == buff)
        goto error;

    return (int)num;

error:
    if (fp) fclose(fp);
    return CM_INVALID_VAL;
}

/*
 * This function will search the directory 'dirname' and return the first child.
 * '.' and '..' is ignored by default
 */
static int dir_get_child(const char *dirname, char *buff, unsigned bufsize, const char *prefix)
{
    struct dirent *entptr = NULL;
    DIR *dirptr;

    buff[0] = 0;

    dirptr = opendir(dirname);
    if (!dirptr)
        return -1;

    while ((entptr = readdir(dirptr))) {
        if (entptr->d_name[0] == '.')
            continue;
        if (prefix && strlen(prefix) && strncmp(entptr->d_name, prefix, strlen(prefix)))
            continue;
        snprintf(buff, bufsize, "%.31s", entptr->d_name);
        break;
    }
    closedir(dirptr);

    return 0;
}

static int conf_get_val(const char *fname, const char *key)
{
    char buff[128] = {'\0'};
    FILE *fp = fopen(fname, "r");
    if (!fp)
        return CM_INVALID_VAL;

    while (fgets(buff, sizeof(buff)-1, fp)) {
        char prefix[128] = {'\0'};
        char tail[128] = {'\0'};
        /* To eliminate cppcheck warnning: Assume string length is no more than 15 */
        sscanf(buff, "%15[^=]=%15s", prefix, tail);
        if (!strncasecmp(prefix, key, strlen(key))) {
            fclose(fp);
            return atoi(tail);
        }
    }

    fclose(fp);
    return CM_INVALID_VAL;
}

static void query_usb_device_info(char *path, struct usb_device_info *p) {
    size_t offset = strlen(path);

    memset(p, 0, sizeof(*p));

    path[offset] = '\0';
    strcat(path, "/idVendor");
    p->idVendor = file_get_value(path, 16);

    if (p->idVendor == CM_INVALID_VAL)
        return;

    path[offset] = '\0';
    strcat(path, "/idProduct");
    p->idProduct = file_get_value(path, 16);

    path[offset] = '\0';
    strcat(path, "/busnum");
    p->busnum = file_get_value(path, 10);

    path[offset] = '\0';
    strcat(path, "/devnum");
    p->devnum = file_get_value(path, 10);

    path[offset] = '\0';
    strcat(path, "/bNumInterfaces");
    p->bNumInterfaces = file_get_value(path, 10);

    path[offset] = '\0';
}

static void query_usb_interface_info(char *path, struct usb_interface_info *p) {
    char driver[128];
    size_t offset = strlen(path);
    int n;

    memset(p, 0, sizeof(*p));

    path[offset] = '\0';
    strcat(path, "/bNumEndpoints");
    p->bInterfaceClass = file_get_value(path, 16);

    path[offset] = '\0';
    strcat(path, "/bInterfaceClass");
    p->bInterfaceClass = file_get_value(path, 16);

    path[offset] = '\0';
    strcat(path, "/bInterfaceSubClass");
    p->bInterfaceSubClass = file_get_value(path, 16);

    path[offset] = '\0';
    strcat(path, "/bInterfaceProtocol");
    p->bInterfaceProtocol = file_get_value(path, 16);

    path[offset] = '\0';
    strcat(path, "/driver");
    n = readlink(path, driver, sizeof(driver));
    if (n > 0) {
        driver[n] = 0;
        n = strlen(driver);
        while (n > 0) {
            if (driver[n] == '/')
                break;
            n--;
        }
        strncpy(p->driver, &driver[n+1], sizeof(p->driver) - 1);
    }

    path[offset] = '\0';
}


/* To detect the device info of the modem.
 * return:
 *  FALSE -> fail
 *  TRUE -> ok
 */
BOOL qmidevice_detect(char *qmichannel, char *usbnet_adapter, unsigned bufsize, PROFILE_T *profile) {
    struct dirent* ent = NULL;
    DIR *pDir;
    const char *rootdir = "/sys/bus/usb/devices";
    struct {
        char path[255*2];
    } *pl;
    pl = (typeof(pl)) malloc(sizeof(*pl));
    memset(pl, 0x00, sizeof(*pl));

    pDir = opendir(rootdir);
    if (!pDir) {
        dbg_time("opendir %s failed: %s", rootdir, strerror(errno));
        goto error;
    }

    while ((ent = readdir(pDir)) != NULL)  {
        char netcard[32+1] = {'\0'};
        char devname[32+5] = {'\0'}; //+strlen("/dev/")
        int netIntf;
        int driver_type;

        if (ent->d_name[0] == 'u')
            continue;

        snprintf(pl->path, sizeof(pl->path), "%s/%s", rootdir, ent->d_name);
        query_usb_device_info(pl->path, &profile->usb_dev);
        if (profile->usb_dev.idVendor == CM_INVALID_VAL)
            continue;

        if (profile->usb_dev.idVendor == 0x2c7c || profile->usb_dev.idVendor == 0x05c6 
		|| profile->usb_dev.idVendor == 0x3763) {
            dbg_time("Find %s/%s idVendor=0x%x idProduct=0x%x, bus=0x%03x, dev=0x%03x",
                rootdir, ent->d_name, profile->usb_dev.idVendor, profile->usb_dev.idProduct,
                profile->usb_dev.busnum, profile->usb_dev.devnum);
        }

        /* get network interface */
        /* NOTICE: there is a case that, bNumberInterface=6, but the net interface is 8 */
        /* toolchain-mips_24kc_gcc-5.4.0_musl donot support GLOB_BRACE */
        /* RG500U's MBIM is at inteface 0 */
        for (netIntf = 0;  netIntf < (profile->usb_dev.bNumInterfaces + 8); netIntf++) {
            snprintf(pl->path, sizeof(pl->path), "%s/%s:1.%d/net", rootdir, ent->d_name, netIntf);
            dir_get_child(pl->path, netcard, sizeof(netcard), NULL);
            if (netcard[0])
                break;
        }


        if (netcard[0] == '\0')
            continue;

        /* not '-i iface' */
        if (usbnet_adapter[0] && strcmp(usbnet_adapter, netcard))
            continue;

        snprintf(pl->path, sizeof(pl->path), "%s/%s:1.%d", rootdir, ent->d_name, netIntf);
        query_usb_interface_info(pl->path, &profile->usb_intf);
        driver_type = get_driver_type(profile);

        if (driver_type == SOFTWARE_ECM_RNDIS_NCM)
        {
            int atIntf = -1;

            if (profile->usb_dev.idVendor == 0x2c7c) { //Quectel
                switch (profile->usb_dev.idProduct) { //EC200U
                case 0x0901: //EC200U
                case 0x8101: //RG801H
                    atIntf = 2;
                break;
                case 0x0900: //RG500U
                    atIntf = 4;
                break;
                case 0x6026: //EC200T
                case 0x6005: //EC200A
                case 0x6002: //EC200S
                case 0x6001: //EC100Y
                    atIntf = 3;
                break;
                default:
                   dbg_time("unknow at interface for USB idProduct:%04x\n", profile->usb_dev.idProduct);
                break;
                }
            }

	if(profile->usb_dev.idVendor == 0x3763 && profile->usb_dev.idProduct == 0x3c93) 
	{
		atIntf = 1;
	}


            if (atIntf != -1) {
                snprintf(pl->path, sizeof(pl->path), "%s/%s:1.%d", rootdir, ent->d_name, atIntf);
                dir_get_child(pl->path, devname, sizeof(devname), "tty");
                if (devname[0] && !strcmp(devname, "tty")) {
                    snprintf(pl->path, sizeof(pl->path), "%s/%s:1.%d/tty", rootdir, ent->d_name, atIntf);
                    dir_get_child(pl->path, devname, sizeof(devname), "tty");
                }
            }
        }
        
        if (netcard[0] && devname[0]) {
            if (devname[0] == '/')
                snprintf(qmichannel, bufsize, "%s", devname);
            else
                snprintf(qmichannel, bufsize, "/dev/%s", devname);
            snprintf(usbnet_adapter, bufsize, "%s", netcard);
            dbg_time("Auto find qmichannel = %s", qmichannel);
            dbg_time("Auto find usbnet_adapter = %s", usbnet_adapter);
            break;
        }
    }
    closedir(pDir);

    if (qmichannel[0] == '\0' || usbnet_adapter[0] == '\0') {
        dbg_time("network interface '%s' or qmidev '%s' is not exist", usbnet_adapter, qmichannel);
        goto error;
    }
    free(pl);
    return TRUE;
error:
    free(pl);
    return FALSE;
}


int get_driver_type(PROFILE_T *profile)
{
  if (profile->usb_intf.bInterfaceClass == USB_CLASS_COMM) {
        switch (profile->usb_intf.bInterfaceSubClass) {
            case USB_CDC_SUBCLASS_ETHERNET:
            case USB_CDC_SUBCLASS_NCM:
                return SOFTWARE_ECM_RNDIS_NCM;
            break;
            default:
            break;
        }
    }
    else if (profile->usb_intf.bInterfaceClass == USB_CLASS_WIRELESS_CONTROLLER) {
        if (profile->usb_intf.bInterfaceSubClass == 1 && profile->usb_intf.bInterfaceProtocol == 3)
            return SOFTWARE_ECM_RNDIS_NCM;
    }

    dbg_time("%s unknow bInterfaceClass=%d, bInterfaceSubClass=%d", __func__,
        profile->usb_intf.bInterfaceClass, profile->usb_intf.bInterfaceSubClass);
    return DRV_INVALID;
}

struct usbfs_getdriver
{
    unsigned int interface;
    char driver[255 + 1];
};

struct usbfs_ioctl
{
    int ifno;       /* interface 0..N ; negative numbers reserved */
    int ioctl_code; /* MUST encode size + direction of data so the
			 * macros in <asm/ioctl.h> give correct values */
    void *data;     /* param buffer (in, or out) */
};



int main()
{
    int ret;
    PROFILE_T *profile = &s_profile;
    char qmichannel[32] = {'\0'};
    char usbnet_adapter[32] = {'\0'};

    dbg_time("Quectel auto find the ttyUSB and interface");

    if (qmidevice_detect(qmichannel, usbnet_adapter, sizeof(qmichannel), profile)) {
        profile->hardware_interface = HARDWARE_USB;
    }
	
	
    return 0;
}
