/*************************************************************************
    > File Name: dns.c
    > Author: bw
    > Mail: wang_biao@126.com 
    > Created Time: 2024年01月23日 星期二 23时28分15秒
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>

int main(int argc, char *argv[])
{ 

	res_init(); 

	int i = 0; 

	for (i = 0;i< _res.nscount; i++) /* res->nscount存储了域名服务器的个数 */ 
	{ 
		struct sockaddr_in addr = _res.nsaddr_list[i]; /* 域名服务器的地址 */ 
	} 
	
	int class = ns_c_in; 
	int type = QUERY; 
	char answer[256]=""; 


//       int res_query(const char *dname, int class, int type,
//	                  unsigned char *answer, int anslen);

	res_query("www.baidu.com", class, type, answer, sizeof(answer)); 
	
	res_close(); 
//	printf("answer=%s", answer);    /* answer中为域名解析的结果 */ 

	for(i=0; i<256; i++)
		printf("%c", answer[i]);
//		printf("0x%x\t", answer[i]);

	printf("\n");
	return 0;
}
