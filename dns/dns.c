/*************************************************************************
    > File Name: dns.c
    > Author: bw
    > Mail: wang_biao@126.com 
    > Created Time: 2024年01月23日 星期二 23时28分15秒
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>

int main(int argc, char *argv[])
{ 
	res_state res;

	printf("%s %d\n",__func__, __LINE__);
	res_ninit(res); 
	printf("%s %d\n",__func__, __LINE__);
	int i = 0; 
	for (i = 0;i< res->nscount;i++) /* res->nscount存储了域名服务器的个数 */ 
	{ 
		struct sockaddr_in addr = res->nsaddr_list[i]; /* 域名服务器的地址 */ 
	} 
	
	printf("%s %d\n",__func__, __LINE__);
	int class = ns_c_in; 
	int type = QUERY; 
	char answer[256]=""; 

	printf("%s %d\n",__func__, __LINE__);

	res_nquery(res, "www.baidu.com", class, type, answer, sizeof(answer)); 
	
	printf("%s %d\n",__func__, __LINE__);
	res_nclose(res); 
	printf("%s %d\n",__func__, __LINE__);
	printf("answer=%s", answer); /* answer中为域名解析的结果 */ 
	return 0;
}
