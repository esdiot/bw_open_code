#define BUF_SIZE 512
#define MAX_PATH_LEN 512
#define USB_DIR_BASE    "/sys/bus/usb/devices"

typedef struct
{
    int idVendor;
    int idProduct;
    int InterfaceNum;
    int usbdev;

    char portname[BUF_SIZE];
    char syspath[BUF_SIZE];
    char busname[BUF_SIZE];

    int bulk_ep_in;
    int bulk_ep_out;
    int wMaxPacketSize;
    int usb_need_zero_package;

    int (* write)(const void *handle, void *pbuf, int size, int portnum);
    int (* read)(const void *handle, void *pbuf, int size, int portnum);
} s_usbdev_t;

int strStartsWith(const char *str, const char *match_str);

// static int get_usbsys_val(const char *sys_filename, int base);

// static int get_busname_by_uevent(const char *uevent, char *busname);

int usb_dev_open(int usb_vid, int usb_pid);


